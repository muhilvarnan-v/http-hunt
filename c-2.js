const Client = require('./client')
const moment = require('moment')
const api = new Client()

async function getChallengeInput () {
  return api.get('/challenge/input')
}

async function postChallengeOutput (count) {
  return api.post('/challenge/output', {output: {count}})
}

function processInput (data) {
  console.log(data)
  let activeCount = 0
  const currentDate = moment()
  data.forEach(({startDate, endDate}) => {
    if (moment(startDate) <= currentDate &&
      (endDate == null || moment(endDate) >= currentDate)) {
      activeCount++
    }
  })

  return activeCount
}

async function init () {
  const res = await getChallengeInput()
  const output = await postChallengeOutput(processInput(res.data))
  console.log(output.data)
}

init()
