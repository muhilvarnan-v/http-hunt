const Client = require('./client');
const moment = require('moment');
const _ = require('lodash');
const api = new Client();

async function getChallengeInput() {
  return api.get('/challenge/input')
}

async function postChallengeOutput(totalValue) {
  return api.post('/challenge/output', {output: {totalValue}})
}

function processInput(data) {
  console.log(data)
  const currentDate = moment()
  const activeProducts = data.filter(({startDate, endDate}) => moment(startDate) <= currentDate && (endDate == null || moment(endDate) >= currentDate))
  console.log(_.sumBy(activeProducts, 'price'))
  return _.sumBy(activeProducts, 'price');
}

async function init() {
  try {
    const res = await getChallengeInput();
    const output = await postChallengeOutput(processInput(res.data))
    console.log(output.data)
  } catch (err) {
    console.log(err)
  }
}

init();
