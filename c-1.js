const Client = require('./client')
const api = new Client()

async function getChallengeInput () {
  return api.get('/challenge/input')
}

async function postChallengeOutput (count) {
  return api.post('/challenge/output', {output: {count}})
}

function processInput (data) {
  console.log(data)
  return data.length
}

async function init () {
  const res = await getChallengeInput()
  const output = await postChallengeOutput(processInput(res.data))
  console.log(output.data)
}

init()

