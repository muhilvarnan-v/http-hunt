const Client = require('./client')
const moment = require('moment')
const _ = require('lodash')
const api = new Client()

async function getChallengeInput () {
  return api.get('/challenge/input')
}

async function postChallengeOutput (totalValue) {
  return api.post('/challenge/output', {output: {...data}})
}

function processInput (data) {
  console.log(data)
  let output = {}

  _.map(_.groupBy(data, 'category'), (data, key) => {
    const currentDate = moment()
    let activeCount = 0
    data.forEach(({startDate, endDate}) => {
      if (moment(startDate) <= currentDate &&
        (endDate == null || moment(endDate) >= currentDate)) {
        activeCount++
      }
    })

    if (activeCount > 0) {
      output[key] = activeCount
    }
  })
  console.log(output)
  return output
}

async function init () {
  try {
    const res = await getChallengeInput()
    const output = await postChallengeOutput(processInput(res.data))
    console.log(output.data)
  } catch (err) {
    console.log(err)
  }
}

init()