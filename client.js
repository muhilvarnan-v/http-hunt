const axios = require('axios')
const {BASE_URL, USER_ID} = require('./config')

const client = function () {
  this.axios = axios.create({
    baseURL: BASE_URL,
    responseType: 'json',
    headers: {
      'userId': USER_ID,
    },
  })
}

client.prototype.post = async function (path, payload) {
  return this.axios.post(path, payload)
}

client.prototype.get = async function (path) {
  return this.axios.get(path)
}

module.exports = client